# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool

__all__ = ['CompanyBranch']


class CompanyBranch(ModelSQL, ModelView):
    """Company Branch"""
    __name__ = 'company.branch'

    party = fields.Many2One('party.party', "Party")
    company = fields.Many2One('company.company', "Company")
    warehouse = fields.Many2One(
        'stock.location', "Warehouse",
        domain=[('type', '=', 'warehouse')])
    address = fields.Many2One('party.address', "Address")
    employees = fields.One2Many('company.employee', 'branch', "Employees")


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'

    branch = fields.Many2One('company.branch', "Company Branch")

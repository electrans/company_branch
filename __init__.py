# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import company


def register():
    Pool.register(
        company.CompanyBranch,
        company.Employee,
        module='electrans_company_branch', type_='model')
